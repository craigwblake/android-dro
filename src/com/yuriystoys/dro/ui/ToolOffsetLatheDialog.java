package com.yuriystoys.dro.ui;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.yuriystoys.dro.DroApplication;
import com.yuriystoys.dro.MachineTypes;
import com.yuriystoys.dro.R;
import com.yuriystoys.dro.axes.Axis;
import com.yuriystoys.dro.core.Dro;
import com.yuriystoys.dro.data.Repository;
import com.yuriystoys.dro.tools.Tool;

public class ToolOffsetLatheDialog extends DialogFragment {

	Tool selectedTool = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		selectedTool = null;

		View view = getActivity().getLayoutInflater().inflate(R.layout._fragment_tool_offset_lathe, null);

		Dro dro = ((DroApplication) getActivity().getApplication()).getDro();

		setUi(view, dro.isInMetricMode());
		fillSpinner();

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(R.string.tool_ofset_title);
		builder.setCancelable(false);
		builder.setView(view);

		// set dialog message
		builder.setCancelable(false)
				.setPositiveButton(R.string.tool_ofset_lathe_ok, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// do nothing. The real handler is below
					}
				}).setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

		// create alert dialog
		Dialog dialog = builder.create();
		dialog.setCanceledOnTouchOutside(false);

		dialog.setOnShowListener(new OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {

				Window window = ((AlertDialog) dialog).getWindow();

				if (getResources().getConfiguration().screenHeightDp < 400)
					window.setLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

				((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(
						new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								if (setOffsets())
									dismiss();
							}
						});

				EditText.OnKeyListener listener = new EditText.OnKeyListener() {

					@Override
					public boolean onKey(View v, int keyCode, KeyEvent event) {
						if (keyCode == KeyEvent.KEYCODE_ENTER) {
							if (setOffsets())
								dismiss();
							return true;
						}
						return false;
					}
				};

				_zOffsetEdit.setOnKeyListener(listener);
			}
		});

		return dialog;
	}

	private void fillSpinner() {
		List<Tool> tools = Repository.open(getActivity()).getTools(MachineTypes.LATHE);

		Tool temp = new Tool();
		temp.setName("-- Select --");
		temp.setOffsetX(0D);
		temp.setOffsetZ(0D);

		tools.add(0, temp);

		ArrayAdapter<Tool> dataAdapter = new ArrayAdapter<Tool>(getActivity(), android.R.layout.simple_spinner_item,
				tools);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		_toolSpinner.setAdapter(dataAdapter);

		_toolSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

				selectedTool = (Tool) parent.getItemAtPosition(position);
				Dro dro = DroApplication.getCurrentInstance().getDro();

				double xOffset = dro.isInMetricMode() ? selectedTool.getOffsetX() * Dro.MM_PER_INCH : selectedTool
						.getOffsetX();

				double zOffset = dro.isInMetricMode() ? selectedTool.getOffsetZ() * Dro.MM_PER_INCH : selectedTool
						.getOffsetZ();

				_xOffsetEdit.setText(dro.getCurrentFormat().format(xOffset));
				_zOffsetEdit.setText(dro.getCurrentFormat().format(zOffset));
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});

		_toolSpinner.setEnabled(tools.size() > 1);
	}

	private boolean setOffsets() {
		double xOffset = 0;
		double zOffset = 0;

		try {
			xOffset = Double.parseDouble(_xOffsetEdit.getEditableText().toString());
			zOffset = Double.parseDouble(_zOffsetEdit.getEditableText().toString());

			if (xOffset + zOffset == 0) {
				Toast.makeText(getActivity(), R.string.tool_offset_invalid, Toast.LENGTH_LONG).show();
				return false;
			}
		} catch (NumberFormatException ex) {
			Toast.makeText(getActivity(), R.string.tool_diameter_invalid, Toast.LENGTH_LONG).show();
			return false;
		}

		Dro dro = ((DroApplication) getActivity().getApplication()).getDro();

		dro.getAxis(Axis.X).setToolOffset(-xOffset);
		dro.getAxis(Axis.Z).setToolOffset(-zOffset);

		// if a tool is selected, tell the DRO which tool it is
		if (selectedTool != null && selectedTool.getId() != null) {
			dro.setSelectedToolId(selectedTool.getId());
		} else
			dro.clearSelectedToolId();

		return true;
	}

	public static void Show(Activity activity) {

		FragmentTransaction ft = activity.getFragmentManager().beginTransaction();
		Fragment prev = activity.getFragmentManager().findFragmentByTag("dialog");
		if (prev != null) {
			ft.remove(prev);
		}
		ft.addToBackStack(null);

		// Create and show the dialog.
		DialogFragment newFragment = new ToolOffsetLatheDialog();
		newFragment.show(ft, "dialog");
	}

	private Spinner _toolSpinner;

	private EditText _xOffsetEdit;
	private EditText _zOffsetEdit;

	private void setUi(View view, boolean metricMode) {

		View temp = null;

		temp = view.findViewById(R.id.xOffsetEdit);
		if (temp != null && temp instanceof EditText) {
			_xOffsetEdit = (EditText) temp;
			_xOffsetEdit.setText(R.string.zero_thousandths);
		}

		temp = view.findViewById(R.id.yOffsetEdit);
		if (temp != null && temp instanceof EditText) {
			_zOffsetEdit = (EditText) temp;
			_zOffsetEdit.setText(R.string.zero_thousandths);
		}

		temp = view.findViewById(R.id.toolSpinner);
		if (temp != null && temp instanceof Spinner) {
			_toolSpinner = (Spinner) temp;

			// TODO: Add Tools Adapter

			_toolSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					_xOffsetEdit.setText(R.string.zero_thousandths);
					_zOffsetEdit.setText(R.string.zero_thousandths);
				}
			});
		}

		Dro dro = DroApplication.getCurrentInstance().getDro();
		String units = view.getContext().getResources().getString(dro.isInMetricMode() ? R.string.mm : R.string.inch);

		((TextView) view.findViewById(R.id.xOffsetUnits)).setText(units);
		((TextView) view.findViewById(R.id.zOffsetUnits)).setText(units);
	}

}
