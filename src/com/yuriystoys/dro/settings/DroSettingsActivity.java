/* Copyright 2012-2013 Yuriy Krushelnytskiy.
 *
 * This file is part of YuriysToys.com Touch DRO
 *
 * Touch DRO is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

package com.yuriystoys.dro.settings;

import java.util.Map;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.util.Log;

import com.yuriystoys.dro.DroApplication;
import com.yuriystoys.dro.R;
import com.yuriystoys.dro.axes.AxisSettings;
import com.yuriystoys.dro.core.Dro;
import com.yuriystoys.dro.data.Repository;

public class DroSettingsActivity extends Activity implements OnSharedPreferenceChangeListener {

	protected PrefsFragment prefsFragment = null;
	protected int currentBankId = -1;
	protected boolean ignoreEvents = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		prefsFragment = new PrefsFragment();

		getFragmentManager().beginTransaction().replace(android.R.id.content, prefsFragment).commit();
	}

	@Override
	protected void onResume() {
		super.onResume();

		PreferenceScreen screen = prefsFragment.getPreferenceScreen();
		SharedPreferences prefs = prefsFragment.getPreferenceScreen().getSharedPreferences();

		// Set up a listener whenever a key changes
		prefs.registerOnSharedPreferenceChangeListener((OnSharedPreferenceChangeListener) this);

		// set summaries for the axes to show the current value
		this.onSharedPreferenceChanged(prefs, PreferenceKeys.X_TPI);
		this.onSharedPreferenceChanged(prefs, PreferenceKeys.Y_TPI);
		this.onSharedPreferenceChanged(prefs, PreferenceKeys.Z_TPI);
		this.onSharedPreferenceChanged(prefs, PreferenceKeys.W_TPI);
		this.onSharedPreferenceChanged(prefs, PreferenceKeys.USE_USB);
		this.onSharedPreferenceChanged(prefs, PreferenceKeys.W_ENABLE);

		this.currentBankId = Integer.parseInt(prefs.getString(PreferenceKeys.PREFERENCE_BANK, "0"));

		if (!DroApplication.isTablet(this)) {
			PreferenceCategory category = (PreferenceCategory) screen.findPreference("general_section");
			Preference pref = category.findPreference(PreferenceKeys.DONT_USE_PANES);

			if (pref != null)
				category.removePreference(pref);
		}

		setSectionTitle();
	}

	protected void setSectionTitle() {
		SharedPreferences prefs = prefsFragment.getPreferenceScreen().getSharedPreferences();

		String title = prefs.getString(PreferenceKeys.PREFERENCE_BANK_NAME, null);

		if (title == null || title.trim().length() == 0) {
			title = getResources().getStringArray(R.array.SettingBankNames)[currentBankId];
		}

		// save current preferences to the repository
		PreferenceCategory prefCat = (PreferenceCategory) prefsFragment.findPreference("general_section");

		prefCat.setTitle(getResources().getString(R.string.general_section_title) + " | " + title);
	}

	@Override
	protected void onPause() {
		super.onPause();

		SharedPreferences prefs = prefsFragment.getPreferenceScreen().getSharedPreferences();

		prefs.unregisterOnSharedPreferenceChangeListener((OnSharedPreferenceChangeListener) this);

		// TODO: Make sure this is safe here
		((DroApplication) this.getApplication()).loadPreferences(prefs);
	}

	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

		if (ignoreEvents)
			return;

		try {

			ignoreEvents = true;

			// Let's do something a preference value changes
			if (key.equals(PreferenceKeys.X_TPI) || key.equals(PreferenceKeys.Y_TPI)
					|| key.equals(PreferenceKeys.Z_TPI) || key.equals(PreferenceKeys.W_TPI)) {
				
				setAxisCpiDetails(sharedPreferences, key);

			} else if (key.equals(PreferenceKeys.USE_USB)) {
				prefsFragment.findPreference(PreferenceKeys.USB_BAUD_RATE).setEnabled(
						sharedPreferences.getBoolean(key, false));
			} else if (key.equals(PreferenceKeys.PREFERENCE_BANK_NAME)) {
				setSectionTitle();
			}

			/*
			 * else if (key.equals(PreferenceKeys.W_ENABLE)) { boolean enable =
			 * sharedPreferences.getBoolean(key, false);
			 * 
			 * prefsFragment.findPreference(PreferenceKeys.W_INVERT).setEnabled(
			 * enable);
			 * prefsFragment.findPreference(PreferenceKeys.W_TARGET).setEnabled
			 * (enable);
			 * prefsFragment.findPreference(PreferenceKeys.W_TPI).setEnabled
			 * (enable); }
			 */else if (key.equals(PreferenceKeys.PREFERENCE_BANK)) {
				// need to switch the workspace and swap the preferences

				DroApplication app = ((DroApplication) getApplication());

				app.setCurrentWorkspace(null);

				Dro dro = app.getDro();

				// clear any tool offsets
				for (AxisSettings axis : dro.getAxes()) {
					axis.clearToolOffset();
				}

				int newBankId = Integer.parseInt(sharedPreferences.getString(key, "0"));

				Map<String, ?> allPrefs = sharedPreferences.getAll();

				// save all current preferences

				Repository repo = Repository.open(DroSettingsActivity.this);
				boolean dontShowWarning = false;

				for (String preferenceKey : allPrefs.keySet()) {
					if (preferenceKey.equalsIgnoreCase(PreferenceKeys.PREFERENCE_BANK)) {
						repo.setPreference(currentBankId, preferenceKey, Integer.toString(currentBankId));
					} 
					else if(preferenceKey.equalsIgnoreCase(PreferenceKeys.DONT_SHOW_SETUP_REMINDER)){
						dontShowWarning = sharedPreferences.getBoolean(PreferenceKeys.DONT_SHOW_SETUP_REMINDER, false);
					}
					else {
						repo.setPreference(currentBankId, preferenceKey, allPrefs.get(preferenceKey).toString());
					}
				}

				// load the new preference set

				SharedPreferences.Editor editor = sharedPreferences.edit();
				editor.clear();

				// reset everything to defaults
				PreferenceManager.setDefaultValues(DroSettingsActivity.this, R.xml.preferences, true);
				editor.putString(PreferenceKeys.PREFERENCE_BANK, Integer.toString(newBankId));

				for (String preferenceKey : allPrefs.keySet()) {
					
					//don't show reminder should be bank-independent
					if(preferenceKey.equals(PreferenceKeys.DONT_SHOW_SETUP_REMINDER)){
						editor.putBoolean(preferenceKey, dontShowWarning);
					}
					
					String value = repo.getPreference(newBankId, preferenceKey);

					if (value != null) {
						Log.d("loaded", preferenceKey + ": " + value);

						Preference pref = prefsFragment.findPreference(preferenceKey);

						if (pref instanceof EditTextPreference || pref instanceof ListPreference) {
							editor.putString(preferenceKey, value);
						} else if (pref instanceof CheckBoxPreference) {
							editor.putBoolean(preferenceKey, Boolean.parseBoolean(value));
						}
					}
				}

				editor.commit();

				// set the current bank ID to the new value
				// finish();
				// startActivity(getIntent());

				currentBankId = newBankId;

				prefsFragment.setPreferenceScreen(null);
				prefsFragment.addPreferencesFromResource(R.xml.preferences);
				

				setSectionTitle();
				
				setAxisCpiDetails(sharedPreferences, PreferenceKeys.X_TPI);
				setAxisCpiDetails(sharedPreferences, PreferenceKeys.Y_TPI);
				setAxisCpiDetails(sharedPreferences, PreferenceKeys.Z_TPI);
				setAxisCpiDetails(sharedPreferences, PreferenceKeys.W_TPI);
			}
		} finally {
			ignoreEvents = false;
		}
	}
	
	private void setAxisCpiDetails(SharedPreferences sharedPreferences, String key)
	{
		double newValue = 0;

		try {
			String keyValue = sharedPreferences.getString(key, null);

			if (keyValue != null) {
				newValue = Double.parseDouble(keyValue);
				prefsFragment.findPreference(key).setSummary(
						getResources().getString(R.string.axis_tpi_summary, newValue));
			} else {
				prefsFragment.findPreference(key).setSummary(
						getResources().getString(R.string.axis_tpi_summary_not_set, Dro.DEFAULT_CPI));
			}

		} catch (NumberFormatException nfex) {
			prefsFragment.findPreference(key).setSummary(
					getResources().getString(R.string.axis_tpi_summary_not_set, Dro.DEFAULT_CPI));
		}
	}

	public static class PrefsFragment extends PreferenceFragment {

		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);

			// Load the preferences from an XML resource
			addPreferencesFromResource(R.xml.preferences);
		}
	}
}
