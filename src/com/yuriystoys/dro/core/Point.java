/* Copyright 2012-2013 Yuriy Krushelnytskiy.
 *
 * This file is part of YuriysToys.com Touch DRO
 *
 * Touch DRO is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

package com.yuriystoys.dro.core;

import java.text.ParseException;
import java.util.Date;

import android.database.Cursor;

import com.yuriystoys.dro.data.Repository;

public class Point {

	private Integer id; // ID can be null on new points
	private int workspaceId;
	private Integer toolId; // It's OK for the tool to be null (if the tool is
							// not set)
	private String name;
	private String description; // Description can be null (it's optional)
	private int x;
	private int y;
	private Integer z;
	private Date createdOn;
	private Date updatedOn;

	public Integer getId() {
		return id;
	}

	public void setId(int id) {
		if (this.id != null)
			throw new IllegalStateException("Only initial ID can be set");

		this.id = id;
	}

	public int getWorkspaceId() {
		return workspaceId;
	}

	public Integer getToolId() {
		return toolId;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public Integer getZ() {
		return z;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	// Hide the default constructor
	private Point() {
	}

	/**
	 * Creates a point using three axes
	 * 
	 * @param name
	 * @param description
	 * @param x
	 * @param y
	 * @param z
	 * @param toolId
	 * @return
	 */
	public static Point createPoint3D(String name, String description, int x, int y, int z, Integer toolId) {
		Point point = createPoint2D(name, description, x, y, toolId);
		point.z = z;
		return point;
	}

	/**
	 * Creates a point using only X and Y positions
	 * 
	 * @param name
	 * @param description
	 * @param x
	 * @param y
	 * @param toolId
	 * @return
	 */
	public static Point createPoint2D(String name, String description, int x, int y, Integer toolId) {
		Point point = new Point();

		point.name = name != null ? name : "";
		point.description = description;
		point.x = x;
		point.y = y;
		point.toolId = toolId;

		return point;
	}

	public void updatePoint3D(String name, String description, int x, int y, int z, Integer toolId) {
		updatePoint2D(name, description, x, y, toolId);
		this.z = z;
	}

	public void updatePoint2D(String name, String description, int x, int y, Integer toolId) {

		this.name = name != null ? name : "";
		this.description = description;
		this.x = x;
		this.y = y;
		this.z = null;
		this.toolId = toolId;
	}

	public Point(Cursor c) throws ParseException {
		this.id = c.getInt(c.getColumnIndex(Repository.POINT_ID));
		this.workspaceId = c.getInt(c.getColumnIndex(Repository.POINT_WORKSPACE_ID));
		this.toolId = (!c.isNull(c.getColumnIndex(Repository.POINT_TOOL_ID)) ? c.getInt(c
				.getColumnIndex(Repository.POINT_TOOL_ID)) : null);
		this.name = c.getString(c.getColumnIndex(Repository.POINT_NAME));
		this.description = (!c.isNull(c.getColumnIndex(Repository.POINT_DESCRIPTION)) ? c.getString(c
				.getColumnIndex(Repository.POINT_DESCRIPTION)) : null);
		this.x = c.getInt(c.getColumnIndex(Repository.POINT_X));
		this.y = c.getInt(c.getColumnIndex(Repository.POINT_Y));
		this.z = (!c.isNull(c.getColumnIndex(Repository.POINT_Z)) ? c.getInt(c.getColumnIndex(Repository.POINT_Z))
				: null);
		this.createdOn = (!c.isNull(c.getColumnIndex(Repository.POINT_CREATED_ON)) ? Repository.iso8601Format.parse(c
				.getString(c.getColumnIndex(Repository.POINT_CREATED_ON))) : null);
		this.updatedOn = (!c.isNull(c.getColumnIndex(Repository.POINT_UPDATED_ON)) ? Repository.iso8601Format.parse(c
				.getString(c.getColumnIndex(Repository.POINT_UPDATED_ON))) : null);
	}

}
