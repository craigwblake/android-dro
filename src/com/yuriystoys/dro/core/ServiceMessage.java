package com.yuriystoys.dro.core;

public class ServiceMessage {

	public static final int STATE_CHANGE = 1;
	public static final int POSITION_READ = 2;
	public static final int DEVICE_CONNECTED = 3;
	public static final int TOAST = 4;
}
