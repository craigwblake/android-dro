/* Copyright 2012-2013 Yuriy Krushelnytskiy.
 *
 * This file is part of YuriysToys.com Touch DRO
 *
 * Touch DRO is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

package com.yuriystoys.dro.tools;

import java.text.ParseException;

import android.database.Cursor;

import com.yuriystoys.dro.MachineTypes;
import com.yuriystoys.dro.data.Repository;

public class Tool {

	@Override
	public String toString() {
		return this.name != null && this.name.trim().length() > 0 ? this.name : super.toString();
	}

	private Integer id;
	private MachineTypes type;
	private String name;
	private String description;
	private double fluteDiameter;
	private double shankDiameter;
	private double fluteLength;
	private int toothCount;
	private double offsetX;
	private double offsetY;
	private double offsetZ;

	public final static int FACTOR = 10000;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public MachineTypes getType() {
		return type;
	}

	public void setType(MachineTypes type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getFluteDiameter() {
		return fluteDiameter;
	}

	public void setFluteDiameter(double fluteDiameter) {
		this.fluteDiameter = fluteDiameter;
	}

	public double getShankDiameter() {
		return shankDiameter;
	}

	public void setShankDiameter(double shankDiameter) {
		this.shankDiameter = shankDiameter;
	}

	public double getFluteLength() {
		return fluteLength;
	}

	public void setFluteLength(double fluteLength) {
		this.fluteLength = fluteLength;
	}
	
	public int getToothCount() {
		return toothCount;
	}

	public void setToothCount(int toothCount) {
		this.toothCount = toothCount;
	}

	public double getOffsetX() {
		return offsetX;
	}

	public void setOffsetX(double offsetX) {
		this.offsetX = offsetX;
	}

	public double getOffsetY() {
		return offsetY;
	}

	public void setOffsetY(double offsetY) {
		this.offsetY = offsetY;
	}

	public double getOffsetZ() {
		return offsetZ;
	}

	public void setOffsetZ(double offsetZ) {
		this.offsetZ = offsetZ;
	}
	
	public Tool() {}

	public Tool(Cursor c) throws ParseException {
		this.id = c.getInt(c.getColumnIndex(Repository.TOOL_ID));
		this.type = MachineTypes.values()[c.getInt(c.getColumnIndex(Repository.TOOL_TYPE))];
		this.name = c.getString(c.getColumnIndex(Repository.TOOL_NAME));

		this.description = !c.isNull(c.getColumnIndex(Repository.TOOL_DESCRIPTION)) ? c.getString(c
				.getColumnIndex(Repository.TOOL_DESCRIPTION)) : null;

		this.fluteDiameter = (!c.isNull(c.getColumnIndex(Repository.TOOL_FLUTE_DIA)) ? c.getInt(c
				.getColumnIndex(Repository.TOOL_FLUTE_DIA)) : 0) / (double)FACTOR;
		this.shankDiameter = (!c.isNull(c.getColumnIndex(Repository.TOOL_SHANK_DIA)) ? c.getInt(c
				.getColumnIndex(Repository.TOOL_SHANK_DIA)) : 0) / (double)FACTOR;
		this.fluteLength = (!c.isNull(c.getColumnIndex(Repository.TOOL_FLUTE_LENGTH)) ? c.getInt(c
				.getColumnIndex(Repository.TOOL_FLUTE_LENGTH)) : 0) / (double)FACTOR;
		
		this.toothCount = c.getInt(c.getColumnIndex(Repository.TOOL_TOOTH_COUNT));

		this.offsetX = (!c.isNull(c.getColumnIndex(Repository.TOOL_X_OFFSET)) ? c.getInt(c
				.getColumnIndex(Repository.TOOL_X_OFFSET)) : 0) / (double)FACTOR;
		this.offsetY = (!c.isNull(c.getColumnIndex(Repository.TOOL_Y_OFFSET)) ? c.getInt(c
				.getColumnIndex(Repository.TOOL_Y_OFFSET)) : 0) / (double)FACTOR;
		this.offsetZ = (!c.isNull(c.getColumnIndex(Repository.TOOL_Z_OFFSET)) ? c.getInt(c
				.getColumnIndex(Repository.TOOL_Z_OFFSET)) : 0) / (double)FACTOR;

	}


}
